import Router from 'vue-router'
import Vuetify from 'vuetify'

const plugins = [Router, Vuetify]

export default function(Vue) {
    plugins.forEach(plugin => {
        if (!plugin.special) {
            Vue.use(plugin)
        } else {
            Vue.use(plugin.plugin, plugin.options)
        }
    })
}