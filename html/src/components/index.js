const components = {

}

export default function(Vue) {
  Object.keys(components).forEach(component => {
    const viewComponent = components[component]
    Vue.component(component, viewComponent)
  })
}
