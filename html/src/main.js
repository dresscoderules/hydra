// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import AttachPluginsOnto from '@/plugins'
import AttachComponentsOnto from '@/components'
import 'vuetify/dist/vuetify.css'

import App from './App'
import router from './router'
AttachPluginsOnto(Vue)
AttachComponentsOnto(Vue)

Vue.config.productionTip = false

/* eslint-disable no-new */
window.main = new Vue({
    el: '#app',
    router,
    template: '<App/>',
    components: {
        App
    }
})