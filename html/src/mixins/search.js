export default {
    computed: {
        items() {
            const query = this.query.toLowerCase(),
                match = [],
                merchants = this.merchants
            if (query === "")
                return merchants
            return this.merchants.filter(({
                name
            }) => name.toLowerCase().includes(query))
        }
    },
    methods: {
        urlEncode(str) {
            if (str) {
                return str.toString().split("&").map(part => encodeURIComponent(part)).join("%26")
            }
            return ""
        },
        altLink(name, id) {

            this.selected = {
                link: `http://imports.dresscoderules.com:333/hydra/#/${this.prefix}${name}-${id}.xml.gz`,
                name,
                id
            }
            this.dialog = true
            this.loading = true
            fetch(`http://imports.dresscoderules.com:333/${this.python_filename}.py?id=${this.urlEncode(id)}&name=${this.urlEncode(name)}`).then(a => a.json()).then(resp => {
                if (resp.success) {
                    this.loading = false
                    this.$router.push(`/${this.prefix}${this.urlEncode(name)}-${this.urlEncode(id)}.xml.gz`)
                }
            }).catch(err => {
                console.log(err)
                this.error = true
            })
        }
    }
}