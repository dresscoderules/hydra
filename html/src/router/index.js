import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/views/'
import ProductCategoriesGenerator from '@/views/Product-Categories-Generator'
import VigLinkImporter from '@/views/imports/vigLink'
import ShopStyleImporter from '@/views/imports/shopStyle'
import PepperJamImporter from '@/views/imports/pepperJam'
import CJImporter from '@/views/imports/commissionJunction'
import ViewProducts from '@/views/View-Products'
import BanWord from '@/views/Ban-Word'

export default new Router({
    routes: [{
        path: '/',
        name: 'Main',
        component: Index
    }, {
        path: '/Product-Categories-Generator',
        name: 'Product Categories Generator',
        component: ProductCategoriesGenerator
    }, {
        path: '/imports/vigLink',
        name: 'VigLink Importer',
        component: VigLinkImporter
    }, {
        path: '/imports/shopStyle',
        name: 'ShopStyle Importer',
        component: ShopStyleImporter
    }, {
        path: '/imports/pepperJam',
        name: 'PepperJam Importer',
        component: PepperJamImporter
    }, {
        path: '/imports/commissionJunction',
        name: 'Commision Junction Importer',
        component: CJImporter
    }, {
        path: '/View-Products/:filename',
        name: 'View Products',
        component: ViewProducts,
        props: true
    }, {
        path: '/Ban-Word',
        name: 'Ban Word',
        component: BanWord
    }]
})