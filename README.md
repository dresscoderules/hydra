# Hydra
This is an aggregator for ClickJunction, Linkshare, VigLink and Target for importing clothes into WordPress
![Main Home](screenshots/main.png)
![Show Items](screenshots/items.png)
![Generate categories](screenshots/gen.png)

# Technical Structure

Each Directory in this folder contains a `README.md` file that describes the purpose of the folder.

## Purpose of each file in this directory

 * ``current_imports.py``
 	* Returns to the frontend a `JSON` object of all data required to:
 		* Make Imports
 		* Regenerate Imports
 		* View Products of Imports
	* Does not output any information itself
 * ``view_products.py``
	* Accepts a `file id` which corresponds to a file within the `raw_xml` folder
	* Returns to the frontend a `JSON` object of all data required to:
		* View Information of each product
	* Does not output any information itself
 * ``import_products.py``
	* Accepts:
		* a `file id` which corresponds to a file within the `raw_xml` folder
		* a `JSON` object which contains all of the selected ids 
	* Returns to the frontend a `JSON` object indicating if import was successful
 * ``raw_xml/``
	* Stores all of the raw xml files that were downloaded
	* Will be read from in order to produce ``view_products.py`` output
 * ``xml/``
 	* Stores all of the wp ready import files
 * ``selections/``
 	* Stores all of the selections made to be imported
 	* File corresponds to raw_xml file name except does not end in `.xml.gz` but ends in `.json`
 	