# Importers
Each file in this directory is to import from an affliate provider and save that file into /raw_xml doing any parsing or filtering that it needs to.

Each file is to implement these methods and have no output other than what method is called:

 * download(``dictionary of all the data it needs``)
 	* this method will output a `.xml.gz` into the `raw_xml` directory to later be processed
 	* this method returns 0 if the task is completed and 1 if an issue occurs (catching all errors within)
 * extract_xml_for_viewing(``dictionary of all the data it needs``)
 	* this method will return a dictionary of all the data required to view
 	* examples of the minimum it shall output is:
 		* merchant name
 		* affliate name
 		* file id
 		* original file name
 * extract_xml_for_import(``dictionary of all the data it needs``)
 	* this method will return an xml string of the currently importing file
 	